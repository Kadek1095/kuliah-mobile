import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

void main() {
  runApp(MaterialApp(
      home: Scaffold(
          backgroundColor: Colors.black54,
          appBar: AppBar(
            leading: Icon(Icons.menu),
            title: Text("Aplikasi Indra"),
            actions: [
              Icon(MdiIcons.thumbUpOutline),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Icon(MdiIcons.thumbDownOutline),
              ),
              Icon(Icons.more_vert),
            ],
            backgroundColor: Colors.blue.shade900,
          ),
          body: Container(
            padding: EdgeInsets.all(30),
            child: GridView.count(
              crossAxisCount: 2,
              children: <Widget>[
                myMenu(title: "Home", icon: Icons.home),
                myMenu(title: "Account", icon: Icons.account_box),
                myMenu(
                    title: "Announcement", icon: Icons.announcement_outlined),
                myMenu(
                  title: "Cloud",
                  icon: Icons.backup_outlined,
                )
              ],
            ),
          ))));
}

class myMenu extends StatelessWidget {
  myMenu({this.title, this.icon});
  final String title;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(8),
      child: InkWell(
        onTap: () {},
        splashColor: Colors.green,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Icon(
                icon,
                size: 70,
              ),
              Text(title, style: new TextStyle(fontSize: 17))
            ],
          ),
        ),
      ),
    );
  }
}
