import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

void main() {
  runApp(MaterialApp(
      home: Scaffold(
          backgroundColor: Colors.black54,
          appBar: AppBar(
            // leading: Icon(Icons.menu),
            title: Text("Aplikasi Indra"),
            actions: [
              Icon(MdiIcons.thumbUpOutline),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Icon(MdiIcons.thumbDownOutline),
              ),
              Icon(Icons.more_vert),
            ],
            backgroundColor: Colors.blue.shade900,
          ),
          drawer: new Drawer(
            child: ListView(
              children: <Widget>[
                UserAccountsDrawerHeader(
                  accountName: new Text(
                    "KADEK INDRA KUSUMA",
                    style: new TextStyle(fontWeight: FontWeight.bold),
                  ),
                  accountEmail: new Text("indra.kusuma.2@undiksha.ac.id"),
                  currentAccountPicture: CircleAvatar(
                    backgroundImage: NetworkImage(
                        "https://cdn.pixabay.com/photo/2017/07/05/01/51/star-wars-2473201_960_720.png"),
                  ),
                  decoration: new BoxDecoration(color: Colors.blue),
                ),
                ListTile(
                  leading: Icon(Icons.person),
                  title: Text("Profil"),
                ),
                ListTile(
                  leading: Icon(Icons.info),
                  title: Text("Tentang"),
                ),
                ListTile(
                  leading: Icon(Icons.exit_to_app),
                  title: Text("Keluar"),
                ),
              ],
            ),
          ),
          body: Container(
            // new Image(image: NetworkImage(https://drive.google.com/file/d/1UoCn1yjUFnP-dC74SIb9t9P_g61T_QYZ/view?usp=sharing)),
            padding: EdgeInsets.all(30),
            child: GridView.count(
              crossAxisCount: 2,
              children: <Widget>[
                myMenu(
                  title: "Lokasi",
                  icon: Icons.my_location_sharp,
                  warna: Colors.green,
                ),
                myMenu(
                    title: "Joanyar", icon: Icons.store, warna: Colors.yellow),
                myMenu(
                    title: "All Genre",
                    icon: Icons.music_note,
                    warna: Colors.purple),
                myMenu(
                    title: "Undiksa",
                    icon: Icons.location_city,
                    warna: Colors.blue),
              ],
            ),
          ))));
}

class myMenu extends StatelessWidget {
  myMenu({this.title, this.icon, this.warna});
  final String title;
  final IconData icon;
  final MaterialColor warna;
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(8),
      child: InkWell(
        onTap: () {},
        splashColor: Colors.green,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Icon(
                icon,
                size: 70,
                color: warna,
              ),
              Text(title, style: new TextStyle(fontSize: 17))
            ],
          ),
        ),
      ),
    );
  }
}
