import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

void main() {
  runApp(MaterialApp(
      home: Scaffold(
          backgroundColor: Colors.black54,
          appBar: AppBar(
            leading: Icon(Icons.menu),
            title: Text("Aplikasi Indra"),
            actions: [
              Icon(MdiIcons.thumbUpOutline),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Icon(MdiIcons.thumbDownOutline),
              ),
              Icon(Icons.more_vert),
            ],
            backgroundColor: Colors.blue.shade900,
          ),
          body: Center(
            child: Image(
              image: NetworkImage(
                  "https://cdn.pixabay.com/photo/2017/07/05/01/51/star-wars-2473201_960_720.png"),
            ),
          ))));
}
